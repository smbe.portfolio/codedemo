﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace WebAppCore.Models
{
    public class Category
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string CategoryName { get; set; }
        [Required]
        [DisplayName("Display Order")]
        [Range(1,int.MaxValue,ErrorMessage ="Display Order for category must be greater than 0")]
        public string DisplayOrder { get; set; }
    }
}
