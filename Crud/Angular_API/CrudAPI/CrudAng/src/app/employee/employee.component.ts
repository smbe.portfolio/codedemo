import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { EmployeeService } from '../employee.service';  
import { Employee } from '../employee';
import { Observable } from 'rxjs';  

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})
export class EmployeeComponent implements OnInit {

  message = null;
  employeeForm: any;
  employeeIdUpdate = null;
  allEmployees: Observable<Employee[]>;

  constructor(private employeeService:EmployeeService) {

  }

  ngOnInit(): void {
    this.employeeForm = new FormGroup({
      EmpName: new FormControl(),
      DateOfBirth: new FormControl(),
      Emailid: new FormControl(),
      Gender: new FormControl(),
      Address: new FormControl(),
      pinCode: new FormControl()
    }); 
    this.loadAllEmployees();
  }

  loadAllEmployees() {  
    this.allEmployees = this.employeeService.getAllEmployee();  
  }

  onSubmit():void{
    console.log(this.employeeForm.value);
    const employee = this.employeeForm.value;  
    this.CreateEmployee(employee);
    this.employeeForm.reset(); 
  }

  loadEmployeeToEdit(employeeId: number) {  
    this.employeeService.getEmployeeById(employeeId).subscribe(employee=> {    
      this.employeeIdUpdate = employee.EmpID;  
      this.employeeForm.controls['EmpName'].setValue(employee.EmpName);  
      this.employeeForm.controls['DateOfBirth'].setValue(employee.DateOfBirth);  
      this.employeeForm.controls['Emailid'].setValue(employee.Emailid);  
      this.employeeForm.controls['Gender'].setValue(employee.Gender);  
      this.employeeForm.controls['Address'].setValue(employee.Address);  
      this.employeeForm.controls['pinCode'].setValue(employee.pinCode);  
    });  
  
  }

  CreateEmployee(employee: Employee) { 
    if (this.employeeIdUpdate == null) {  
      this.employeeService.createEmployee(employee).subscribe(()=>{
        this.message = "Record Saved!"
        this.loadAllEmployees();
      });
    }else{
      employee.EmpID = this.employeeIdUpdate;  
      this.employeeService.updateEmployee(employee).subscribe(() => {
        this.message = "Record Updated!"  
        this.loadAllEmployees();  
        this.employeeIdUpdate = null;  
        this.employeeForm.reset();  
      });  
    }
  }

  deleteEmployee(employeeId: number) {  
    if (confirm("Are you sure you want to delete this ?")) {   
      this.employeeService.deleteEmployeeById(employeeId).subscribe(() => {
        this.message = "Record Deleted!"   
        this.loadAllEmployees();
      });  
    }  
  }
}
