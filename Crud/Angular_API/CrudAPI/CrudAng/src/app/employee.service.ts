import { Employee } from './employee';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  url = 'https://localhost:44326/Api/Employee/'

  constructor(private http:HttpClient) { 

  }

  getAllEmployee(): Observable<Employee[]> {
    return this.http.get<Employee[]>(this.url + 'AllEmployeeDetails');
  }

  getEmployeeById(employeeId: number): Observable<Employee> {
   return this.http.get<Employee>(this.url + 'GetEmployeeDetailsById/' + employeeId);
 }

 createEmployee(employee: Employee): Observable<Employee> {
   const httpOptions = { headers: new HttpHeaders({'Content-Type':'application/json'})}
   return this.http.post<Employee>(this.url + 'InsertEmployeeDetails/',employee,httpOptions);
 }

 updateEmployee(employee: Employee): Observable<Employee> {
   const httpOptions = { headers: new HttpHeaders({'Content-Type':'application/json'})}
   return this.http.put<Employee>(this.url + 'UpdateEmployeeDetails/',employee,httpOptions);
 }

 deleteEmployeeById(employeeId: number): Observable<Employee> {
   const httpOptions = { headers: new HttpHeaders({'Content-Type':'application/json'})}
   return this.http.delete<Employee>(this.url + 'DeleteEmployeeDetails/' + employeeId,httpOptions);
 }
}
