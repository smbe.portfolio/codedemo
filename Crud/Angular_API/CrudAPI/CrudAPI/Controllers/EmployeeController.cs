﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using CrudAPI.Models;

namespace CrudAPI.Controllers
{
    [RoutePrefix("Api/Employee")]
    public class EmployeeController : ApiController
    {
        CrudAngularEntities objEntity = new CrudAngularEntities();

        [HttpGet]
        [Route("AllEmployeeDetails")]
        public IQueryable<EmployeeDetail> GetEmployee()
        {
            try
            {
                return objEntity.EmployeeDetails;
            }
            catch(Exception)
            {
                throw;
            }
        }

        [HttpGet]
        [Route("GetEmployeeDetailsById/{employeeId}")]
        public IHttpActionResult GetEmployeeDetailsById(int employeeId)
        {
            var empDetails = new EmployeeDetail();

            try
            {
                empDetails = objEntity.EmployeeDetails.Find(employeeId);
                if(empDetails == null)
                {
                    return NotFound();
                }
            }
            catch (Exception)
            {
                throw;
            }

            return Ok(empDetails);
        }

        [HttpPost]
        [Route("InsertEmployeeDetails")]

        public IHttpActionResult PostEmployee(EmployeeDetail data)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                objEntity.EmployeeDetails.Add(data);
                objEntity.SaveChanges();
            }
            catch(Exception ex)
            {
                return base.Content(HttpStatusCode.InternalServerError, ex.ToString());
            }

            return Ok(data);
        }

        [HttpPut]
        [Route("UpdateEmployeeDetails")]

        public IHttpActionResult PutEmployeeMaster(EmployeeDetail employee)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var empDetails = new EmployeeDetail();
                empDetails = objEntity.EmployeeDetails.Find(employee.EmpID);
                if(objEntity != null)
                {
                    empDetails.EmpName = employee.EmpName;
                    empDetails.Address = employee.Address;
                    empDetails.Emailid = employee.Emailid;
                    empDetails.DateOfBirth = employee.DateOfBirth;
                    empDetails.Gender = employee.Gender;
                    empDetails.pinCode = employee.pinCode;
                }

                int i = this.objEntity.SaveChanges();
            }
            catch (Exception)
            {
                throw;
            }

            return Ok(employee);
        }

        [HttpDelete]
        [Route("DeleteEmployeeDetails/{employeeid}")]
        public IHttpActionResult DeleteEmployee(int employeeid)
        {
            EmployeeDetail empDetails = objEntity.EmployeeDetails.Find(employeeid);
            if (empDetails == null)
            {
                return NotFound();
            }

            objEntity.EmployeeDetails.Remove(empDetails);
            objEntity.SaveChanges();

            return Ok(empDetails);
        }
    }
}
