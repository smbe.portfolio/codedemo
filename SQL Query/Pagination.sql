DECLARE @PageNo INT=1
DECLARE @PageSize INT=3

SELECT FruitName,Price,RowNum
FROM
(
	SELECT FruitName,Price,
	ROW_NUMBER() over (ORDER BY Price) as RowNum
	FROM SampleFruits
)T
WHERE
T.RowNum BETWEEN ((@PageNo - 1) * @PageSize) + 1 AND (@PageNo * @PageSize)

/*Creating table with data*/

CREATE TABLE SampleFruits ( 
Id INT PRIMARY KEY IDENTITY(1,1) , 
FruitName VARCHAR(50) , 
Price INT
)
GO
INSERT INTO SampleFruits VALUES('Apple',20)
INSERT INTO SampleFruits VALUES('Apricot',12)
INSERT INTO SampleFruits VALUES('Banana',8)
INSERT INTO SampleFruits VALUES('Cherry',11)
INSERT INTO SampleFruits VALUES('Strawberry',26)
INSERT INTO SampleFruits VALUES('Lemon',4)  
INSERT INTO SampleFruits VALUES('Kiwi',14)  
INSERT INTO SampleFruits VALUES('Coconut',34) 
INSERT INTO SampleFruits VALUES('Orange',24)  
INSERT INTO SampleFruits VALUES('Raspberry',13)
INSERT INTO SampleFruits VALUES('Mango',9)
INSERT INTO SampleFruits VALUES('Mandarin',19)
INSERT INTO SampleFruits VALUES('Pineapple',22)
GO
SELECT * FROM SampleFruits