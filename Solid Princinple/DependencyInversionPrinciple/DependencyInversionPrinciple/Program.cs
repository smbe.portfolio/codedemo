﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DependencyInversionPrinciple
{
    class Program
    {
        static void Main(string[] args)
        {
            Cat cat = new Cat(new CatSpeakBehavior());
            Dog dog = new Dog(new DogSpeakBehavior());

            Console.WriteLine(cat.Speak());
            Console.WriteLine(dog.Speak());

            Console.ReadLine();
        }
    }
}
