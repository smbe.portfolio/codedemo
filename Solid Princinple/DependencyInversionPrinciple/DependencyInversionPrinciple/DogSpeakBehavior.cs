﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DependencyInversionPrinciple
{
    public class DogSpeakBehavior : SpeakBehavior
    {
        public override string Speak()
        {
            return "Woof!";
        }
    }
}
