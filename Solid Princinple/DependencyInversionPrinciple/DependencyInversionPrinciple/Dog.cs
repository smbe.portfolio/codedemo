﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DependencyInversionPrinciple
{
    public class Dog : Animal
    {
        SpeakBehavior behavior;
        public Dog(SpeakBehavior behavior)
        {
            this.behavior = behavior;
        }
        public override string Speak()
        {
            return this.behavior.Speak();
        }
    }
}
