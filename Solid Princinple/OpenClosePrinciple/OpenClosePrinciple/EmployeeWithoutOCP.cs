﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenClosePrinciple
{
    public class EmployeeWithoutOCP
    {
        private int _id { get; set; }
        private string _name { get; set; }
        private string _empType { get; set; }

        public EmployeeWithoutOCP()
        {

        }

        public EmployeeWithoutOCP(int id, string name, string empType)
        {
            this._id = id;
            this._name = name;
            this._empType = empType;
        }

        public decimal CalculateBonus(decimal salary)
        {
            if (this._empType == "Permanent")
            {
                return salary * .1M;
            }
            else
            {
                return salary * .05M;
            }

        }

        public override string ToString()
        {
            return string.Format("ID : {0} Name : {1}", this._id, this._name);
        }
    }
}
