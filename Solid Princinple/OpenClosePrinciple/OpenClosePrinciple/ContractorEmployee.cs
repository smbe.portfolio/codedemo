﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenClosePrinciple
{
    public class ContractorEmployee : Employee
    {
        public ContractorEmployee()
        {

        }

        public ContractorEmployee(int id, string name)
            : base(id, name)
        {

        }

        public override decimal CalculateBonus(decimal salary)
        {
            return salary * .05M;
        }
    }
}
