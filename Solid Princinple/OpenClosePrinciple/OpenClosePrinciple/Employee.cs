﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenClosePrinciple
{
    public abstract class Employee
    {
        private int _id { get; set; }
        private string _name { get; set; }

        public Employee()
        {

        }

        public Employee(int id, string name)
        {
            this._id = id;
            this._name = name;
        }


        public abstract decimal CalculateBonus(decimal salary);

        public override string ToString()
        {
            return string.Format("ID : {0} Name : {1}", this._id, this._name);
        }
    }
}
