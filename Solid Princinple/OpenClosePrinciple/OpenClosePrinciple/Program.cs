﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenClosePrinciple
{
    class Program
    {
        static void Main(string[] args)
        {
            //Without OCP ----------------------------------------------------------

            var john = new EmployeeWithoutOCP(1, "John", "Permanent");

            Console.WriteLine(string.Format("Employee {0} Bonus: {1}",
                john.ToString(),
                john.CalculateBonus(1000).ToString()));


            var jason = new EmployeeWithoutOCP(2, "Jason", "Contractor");

            Console.WriteLine(string.Format("Employee {0} Bonus: {1}",
                jason.ToString(),
                jason.CalculateBonus(2500).ToString()));


            //With OCP ---------------------------------------------------------------

            var jona = new ContractorEmployee(3, "Jona");

            Console.WriteLine(string.Format("Employee {0} Bonus: {1}",
                jona.ToString(),
                jona.CalculateBonus(3000).ToString()));

            var jose = new PermanentEmployee(4, "Jose");

            Console.WriteLine(string.Format("Employee {0} Bonus: {1}",
                jose.ToString(),
                jose.CalculateBonus(4000).ToString()));

            Console.ReadLine();
        }
    }
}
