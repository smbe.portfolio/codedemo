﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiskovPrinciple
{
    public abstract class Animal
    {
        private string _color;
        private double _weight;
        private double _height;

        public abstract void HuntFood();
        public abstract void Rest();

        public virtual void Talk()
        {
            Console.WriteLine("Now talking");
        }

    }
}
