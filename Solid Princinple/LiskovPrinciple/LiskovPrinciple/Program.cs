﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiskovPrinciple
{
    class Program
    {
        static void Main(string[] args)
        {
            var tilapia = new Fish();
            tilapia.HuntFood();
            tilapia.Rest();
            tilapia.Talk();
            tilapia.Swim();

            Console.WriteLine();

            var parot = new Birds();
            parot.HuntFood();
            parot.Rest();
            parot.Talk();
            parot.Fly();
            Console.ReadKey();
        }
    }
}
