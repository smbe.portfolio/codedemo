﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiskovPrinciple
{
    public class Fish : Animal, IActionWaterdAnimal
    {
        public override void HuntFood()
        {
            Console.WriteLine("Now hunting as a fish");
        }

        public override void Rest()
        {
            Console.WriteLine("Now resting as a fish");
        }

        public override void Talk()
        {
            Console.WriteLine("Now talking as a fish");
        }

        public void Swim()
        {
            Console.WriteLine("Now swimming");
        }
    }
}
