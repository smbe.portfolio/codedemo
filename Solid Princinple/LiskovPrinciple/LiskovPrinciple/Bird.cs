﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiskovPrinciple
{
    public class Birds : Animal, IActionWindAnimal
    {
        public override void HuntFood()
        {
            Console.WriteLine("Now hunting as a bird");
        }

        public override void Rest()
        {
            Console.WriteLine("Now resting as a bird");
        }

        public override void Talk()
        {
            Console.WriteLine("Now talking as a bird");
        }

        public void Fly()
        {
            Console.WriteLine("Now Flying");
        }
    }
}
